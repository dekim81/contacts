//
//  AppDelegate.swift
//  contacts
//
//  Created by Dong-Eon Kim on 2020/01/09.
//  Copyright © 2020 Dong-Eon Kim. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let sceneCoordinator = SceneCoordinator(window: window!)
        let storage = ContactMemoryStorage()
        let viewModel = ContactListViewModel(title: "차단 연락처 목록", sceneCoordinator: sceneCoordinator, storage: storage)
        let mainScene = Scene.Main(viewModel)
        sceneCoordinator.transition(to: mainScene, using: .root, animated: false)
        return true
    }
}

