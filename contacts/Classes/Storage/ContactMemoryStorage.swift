//
//  ContactMemoryStorage.swift
//  contacts
//
//  Created by Dong-Eon Kim on 2020/01/09.
//  Copyright © 2020 Dong-Eon Kim. All rights reserved.
//

import Foundation
import RxSwift

class ContactMemoryStorage: ContactStorageType {
    var list = [
        Contact(phoneNumber: "01038974265", insertDate: Date().addingTimeInterval(-10)),
        Contact(phoneNumber: "01094540692", insertDate: Date().addingTimeInterval(-20))
    ]
    
    private lazy var sectionModel = ContactSectionModel(model: 0, items: list)
    private lazy var store = BehaviorSubject<[ContactSectionModel]>(value: [sectionModel])
    
    @discardableResult
    func createContact(phoneNumer: String) -> Observable<Contact> {
        let contact = Contact(phoneNumber: phoneNumer)
        sectionModel.items.insert(contact, at: 0)
        
        store.onNext([sectionModel])
        
        return Observable.just(contact)
    }
    
    @discardableResult
    func deleteContact(contact: Contact) -> Observable<Contact> {
        if let index = sectionModel.items.firstIndex(where: { $0 == contact }) {
            sectionModel.items.remove(at: index)
        }
        
        store.onNext([sectionModel])
        
        return Observable.just(contact)
    }
    
    @discardableResult
    func contactList() -> Observable<[ContactSectionModel]> {
        return store
    }
    
    @discardableResult
    func update(contact: Contact, phoneNumber: String) -> Observable<Contact> {
        let updated = Contact(original: contact, updatePhoneNumber: phoneNumber)
        if let index = sectionModel.items.firstIndex(where: { $0 == contact }) {
            sectionModel.items.remove(at: index)
            sectionModel.items.insert(updated, at: 0)
        }
        
        store.onNext([sectionModel])
        
        return Observable.just(updated)
    }
}
