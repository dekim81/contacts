//
//  ContactStorageType.swift
//  contacts
//
//  Created by Dong-Eon Kim on 2020/01/09.
//  Copyright © 2020 Dong-Eon Kim. All rights reserved.
//

import Foundation
import RxSwift

protocol ContactStorageType {
    @discardableResult
    func createContact(phoneNumer: String) -> Observable<Contact>
    
    @discardableResult
    func deleteContact(contact: Contact) -> Observable<Contact>
    
    @discardableResult
    func update(contact: Contact, phoneNumber: String) -> Observable<Contact>
    
    @discardableResult
    func contactList() -> Observable<[ContactSectionModel]>
}
