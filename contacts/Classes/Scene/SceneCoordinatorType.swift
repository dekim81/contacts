//
//  SceneCoordinatorType.swift
//  contacts
//
//  Created by Dong-Eon Kim on 2020/01/12.
//  Copyright © 2020 Dong-Eon Kim. All rights reserved.
//

import Foundation
import RxSwift

protocol SceneCoordinatorType {
    @discardableResult
    func transition(to scene: Scene, using style: TransitionStype, animated: Bool) -> Completable
    
    @discardableResult
    func close(animated: Bool) -> Completable
}
