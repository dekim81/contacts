//
//  TransitionModel.swift
//  contacts
//
//  Created by Dong-Eon Kim on 2020/01/12.
//  Copyright © 2020 Dong-Eon Kim. All rights reserved.
//

import Foundation


enum TransitionStype {
    case root
    case push
    case modal
}

enum TransitionError: Error {
    case navigationControllerMissing
    case cannotPop
    case unknown
}
