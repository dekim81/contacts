//
//  Scene.swift
//  contacts
//
//  Created by Dong-Eon Kim on 2020/01/12.
//  Copyright © 2020 Dong-Eon Kim. All rights reserved.
//

import UIKit

enum Scene {
    case Main(ContactListViewModel)
    case AddBlockContact(addContactViewModel)
}

extension Scene {
    func instantiate(from storyboard: String = "Main") -> UIViewController {
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        
        switch self {
        case .Main(let viewModel):
            guard let nav = storyboard.instantiateViewController(withIdentifier: "MainNav") as? UINavigationController else {
                fatalError()
            }
            
            guard var mainVC = nav.viewControllers.first as? MainViewController else {
                fatalError()
            }
            
            mainVC.bind(viewModel: viewModel)
            return nav
        case .AddBlockContact(let viewModel):
            guard var addBlockContactVC = storyboard.instantiateViewController(withIdentifier: "AddBlockContactViewController") as? AddBlockContactViewController else {
                fatalError()
            }
        
            addBlockContactVC.bind(viewModel: viewModel)
            return addBlockContactVC
        }
    }
}
