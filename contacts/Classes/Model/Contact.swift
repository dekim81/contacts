//
//  Contact.swift
//  contacts
//
//  Created by Dong-Eon Kim on 2020/01/09.
//  Copyright © 2020 Dong-Eon Kim. All rights reserved.
//

import Foundation
import RxDataSources

struct Contact: Equatable, IdentifiableType {
    var phoneNumber: String
    var insertDate: Date
    var identity: String
    
    init(phoneNumber: String, insertDate: Date = Date()) {
        self.phoneNumber = phoneNumber;
        self.insertDate = insertDate;
        self.identity = "\(insertDate.timeIntervalSinceReferenceDate)"
    }
    
    init(original: Contact, updatePhoneNumber: String) {
        self = original
        self.phoneNumber = updatePhoneNumber
    }
}
