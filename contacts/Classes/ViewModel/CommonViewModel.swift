//
//  CommonViewModel.swift
//  contacts
//
//  Created by Dong-Eon Kim on 2020/01/12.
//  Copyright © 2020 Dong-Eon Kim. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


class CommonViewModel: NSObject {
    let title: Driver<String>
    let sceneCoordinator: SceneCoordinatorType
    let storage: ContactStorageType
    
    init(title: String, sceneCoordinator: SceneCoordinatorType, storage: ContactStorageType) {
        self.title = Observable.just(title).asDriver(onErrorJustReturn: "")
        self.sceneCoordinator = sceneCoordinator
        self.storage = storage
    }
}
