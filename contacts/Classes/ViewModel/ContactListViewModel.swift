//
//  ContactListViewModel.swift
//  contacts
//
//  Created by Dong-Eon Kim on 2020/01/09.
//  Copyright © 2020 Dong-Eon Kim. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Action
import RxDataSources
//  23232
typealias ContactSectionModel = AnimatableSectionModel<Int, Contact>

class ContactListViewModel: CommonViewModel {
    let dataSource: RxTableViewSectionedAnimatedDataSource<ContactSectionModel> = {
        let ds = RxTableViewSectionedAnimatedDataSource<ContactSectionModel>(configureCell: {
            (datasource, tableView, indexPath, contact) -> UITableViewCell in
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = contact.phoneNumber
            return cell
        })
        
        ds.canEditRowAtIndexPath = { _, _ in return true }
        return ds
    }()
        
    func performUpdate(contact: Contact) -> Action<String, Void> {
        return Action { input in
            return self.storage.update(contact: contact, phoneNumber: input).map { _ in }
        }
    }
    
    func performCacnel(contact: Contact) -> CocoaAction {
        return CocoaAction {
            return self.storage.deleteContact(contact: contact).map { _ in }
        }
    }
    
    func addBlockContactAction() -> CocoaAction {
        return CocoaAction { _ in
            return self.storage.createContact(phoneNumer: "")
                .flatMap { contact -> Observable<Void> in
                    let contactViweModel = addContactViewModel(title: "추가", sceneCoordinator: self.sceneCoordinator, storage: self.storage, saveAction: self.performUpdate(contact: contact), cancelAction: self.performCacnel(contact: contact))
                    
                    let addScene = Scene.AddBlockContact(contactViweModel)
                    return self.sceneCoordinator.transition(to: addScene, using: .push, animated: true)
                        .asObservable().map { _ in }
            }
        }
    }
}
