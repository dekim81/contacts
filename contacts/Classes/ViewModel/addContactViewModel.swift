//
//  addContactViewModel.swift
//  contacts
//
//  Created by Dong-Eon Kim on 2020/01/12.
//  Copyright © 2020 Dong-Eon Kim. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import NSObject_Rx
import Action

class addContactViewModel: CommonViewModel {
    let saveAction: Action<String, Void>
    let cancelAction: CocoaAction

    func addBlockContactAction(phoneNumber: String) -> CocoaAction {
        return CocoaAction { _ in
            return self.storage.createContact(phoneNumer: phoneNumber)
                .flatMap { contact -> Observable<Void> in
                    return self.sceneCoordinator.close(animated: true).asObservable().map { _ in }
            }
        }
    }
    
    init(title: String, phoneNumber: String? = nil, sceneCoordinator: SceneCoordinatorType, storage: ContactStorageType, saveAction: Action<String, Void>? = nil, cancelAction: CocoaAction? = nil) {
        self.saveAction = Action<String, Void> { input in
            if let action = saveAction {
                action.execute(input)
            }
            
            return sceneCoordinator.close(animated: true).asObservable().map { _ in }
        }
        
        self.cancelAction = CocoaAction {
            if let action = cancelAction {
                action.execute()
            }
            
            return sceneCoordinator.close(animated: true).asObservable().map { _ in }
        }
        
        super.init(title: title, sceneCoordinator: sceneCoordinator, storage: storage)
    }

}
